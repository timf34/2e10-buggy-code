const int in1 = 17;
const int in2 = 19;
const int in3 = 2;
const int in4 = 3;
const int enA = 16;
const int enB = 5;

void setup() {
  Serial.begin(9600);
  pinMode( in1, OUTPUT );
  pinMode( in2, OUTPUT );
  pinMode( in3, OUTPUT );
  pinMode( in4, OUTPUT );
  pinMode(enA, OUTPUT);
  pinMode(enB, OUTPUT);
}

void loop() {
  /*
  //LEFT FORWARD AT 3/4 SPEED
  //controlling speed 
  analogWrite(enA,191);
  //control direction
  digitalWrite(in1,HIGH);
  digitalWrite(in2,LOW);
  //RIGHT FORWARD AT 3/4 SPEED
  //controlling speed 
  analogWrite(enB,191);
  //control direction
  digitalWrite(in3,LOW);
  digitalWrite(in4,HIGH);
*/
  //LEFT FORWARD AT 1/2 SPEED
  //controlling speed 
  analogWrite(enA,127);
  //control direction
  digitalWrite(in1,HIGH);
  digitalWrite(in2,LOW);
/*  
  //RIGHT FORWARD AT 1/2 SPEED
  //controlling speed
  analogWrite(enB,127);
  //control direction
  digitalWrite(in3,LOW);
  digitalWrite(in4,HIGH);
  //LEFT BACKWARD AT 3/4 SPEED
  //controlling speed 
  analogWrite(enA,191);
  //control direction
  digitalWrite(in1,LOW);
  digitalWrite(in2,HIGH);
  //RIGHT BACKWARD AT 3/4 SPEED
  //controlling speed 
  analogWrite(enB,191);
  //control direction
  digitalWrite(in3,HIGH);
  digitalWrite(in4,LOW);
/*
  //LEFT BACKWARD AT 1/2 SPEED
  //controlling speed 
  analogWrite(enA,127);
  //control direction
  digitalWrite(in1,LOW);
  digitalWrite(in2,HIGH);
  //RIGHT BACKWARD AT 1/2 SPEED
  //controlling speed 
  analogWrite(enB,127);
  //control direction
  digitalWrite(in3,HIGH);
  digitalWrite(in4,LOW);*/
}

const int in1 = 17;
const int in2 = 19;
const int in3 = 2;
const int in4 = 3;
const int enA = 16;
const int enB = 5;

void setup() {
  Serial.begin(9600);
  pinMode( in1, OUTPUT );
  pinMode( in2, OUTPUT );
  pinMode( in3, OUTPUT );
  pinMode( in4, OUTPUT );
  pinMode(enA, OUTPUT);
  pinMode(enB, OUTPUT);
}

void loop() {
 /* 
  //SOFT LEFT
  //direction forwards
  digitalWrite(in1,HIGH);
  digitalWrite(in2,LOW);
  digitalWrite(in3,LOW);
  digitalWrite(in4,HIGH);
  //speed control
  analogWrite(enA,100);
  analogWrite(enB, 255);
  
  //SOFT RIGHT
  //direction forwards
  digitalWrite(in1,HIGH);
  digitalWrite(in2,LOW);
  digitalWrite(in3,LOW);
  digitalWrite(in4,HIGH);
  //speed control
  analogWrite(enB,100);
  analogWrite(enA, 255);
  
  //HARD LEFT
  //direction forwards
  digitalWrite(in1,HIGH);
  digitalWrite(in2,LOW);
  digitalWrite(in3,LOW);
  digitalWrite(in4,HIGH);
  //speed control
  analogWrite(enA,0);
  analogWrite(enB, 255);
  //HARD RIGHT
  //direction forwards
  digitalWrite(in1,HIGH);
  digitalWrite(in2,LOW);
  digitalWrite(in3,LOW);
  digitalWrite(in4,HIGH);
  //speed control
  analogWrite(enB,0);
  analogWrite(enA, 255);
*/
  //HARD REVERSE LEFT
  //direction reverse
  digitalWrite(in1,LOW);
  digitalWrite(in2,HIGH);
  digitalWrite(in3,HIGH);
  digitalWrite(in4,LOW);
  //speed control
  analogWrite(enA,0);
  analogWrite(enB, 255);
/*
  //SOFT REVERSE RIGHT
  //direction reverse
  digitalWrite(in1,LOW);
  digitalWrite(in2,HIGH);
  digitalWrite(in3,HIGH);
  digitalWrite(in4,LOW);
  //speed control
  analogWrite(enA,255);
  analogWrite(enB, 100);
  */
}
